package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] state;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.state = new CellState[rows][columns];
        for (int r=0; r<rows; r++) {
            for (int c=0; c<columns; c++) {
                state[r][c] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        state[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return state[row][column];
    }

    @Override
    public IGrid copy() {

        IGrid newGrid = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        
        for (int r=0; r<numRows(); r++) {
            for (int c=0; c<numColumns(); c++) {
                newGrid.set(r, c, state[r][c]);
            }
        }

        return newGrid;
    }
    
}
