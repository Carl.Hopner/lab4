package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;
import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int r=0;r<numberOfRows();r++) {
			for (int c=0;c<numberOfColumns();c++) {
				nextGeneration.set(r, c, getNextCell(r, c));	
			}
		}
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // A living cell becomes dying:
        if (currentGeneration.get(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        // A dying cell dies:
        else if (currentGeneration.get(row, col) == CellState.DYING) {
            return CellState.DEAD;
        }
        // A dead cell becomes alive if it has exactly 2 living neighbours, stays dead otherwise:
        else {
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                return CellState.ALIVE;
            }
            else {
                return CellState.DEAD;
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    // This method is from GameOfLife.java:
    private int countNeighbors(int row, int col, CellState state) {
		int NumberOfNeighbours = 0;

		for (int r = row-1; r <= row+1; r++) {
			for (int c = col-1; c <= col+1; c++) {
				if (r >= currentGeneration.numRows() | c >= currentGeneration.numColumns() | r<0 | c<0) {
					continue;
				}
				if (r==row && c==col) {
					continue;
				}
				else if (state == currentGeneration.get(r, c)) {
					NumberOfNeighbours++;
				}
			}
		}

		return NumberOfNeighbours;
	}
    
}
